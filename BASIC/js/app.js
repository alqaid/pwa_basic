﻿// Copyright 2016 Google Inc.
// 
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
// 
//      http://www.apache.org/licenses/LICENSE-2.0
// 
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


(function() {
  'use strict';

  var app = {
    isLoading: true,
    spinner: document.querySelector('.loader'),
    container: document.querySelector('.main'),
	  versionModalText: document.getElementById('versionModalText')   // LLAMADA AL ID
	
  };


  /*****************************************************************************
   *
   * EVENTOS
   *
   ****************************************************************************/


  
   document.getElementById('butAdd').addEventListener('click', function() {
	 $('#versionModal').modal('show');
  });

  document.getElementById('butRefresh').addEventListener('click', function() {
    // Refresh all of the forecasts
    
  });

 /*****************************************************************************
   *
   * FUNCIONES
   *
   ****************************************************************************/ 
   
  /*****************************************************************************
   *
   * Methods to update/refresh the UI
   *
   ****************************************************************************/
 

   if (app.isLoading) {
    app.spinner.setAttribute('hidden', true);
    app.container.removeAttribute('hidden');
    app.isLoading = false;

    app.versionModalText.innerHTML = "Versión 2018.07.5.1";
	   
    app.container.removeAttribute('hidden');
      
    app.isLoading = false;
     
      
    
    }
  
})();
